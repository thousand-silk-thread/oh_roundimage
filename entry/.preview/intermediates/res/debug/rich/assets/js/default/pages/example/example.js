/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "E:\\Projects\\HarmonyProject\\OhRoundImage\\entry\\src\\main\\js\\default\\pages\\example\\example.hml?entry");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./lib/json.js!./lib/style.js!E:\\Projects\\HarmonyProject\\OhRoundImage\\entry\\src\\main\\js\\default\\pages\\example\\example.css":
/*!********************************************************************************************************************************!*\
  !*** ./lib/json.js!./lib/style.js!E:/Projects/HarmonyProject/OhRoundImage/entry/src/main/js/default/pages/example/example.css ***!
  \********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
  ".container": {
    "display": "flex",
    "flexDirection": "column",
    "justifyContent": "center",
    "alignItems": "center",
    "left": "0px",
    "top": "0px",
    "width": "100%",
    "height": "100%"
  },
  ".title": {
    "fontSize": "30px",
    "textAlign": "center",
    "width": "200px",
    "height": "100px"
  }
}

/***/ }),

/***/ "./lib/json.js!./lib/style.js!E:\\Projects\\HarmonyProject\\OhRoundImage\\entry\\src\\main\\js\\default\\pages\\roundimage\\roundimage.css":
/*!**************************************************************************************************************************************!*\
  !*** ./lib/json.js!./lib/style.js!E:/Projects/HarmonyProject/OhRoundImage/entry/src/main/js/default/pages/roundimage/roundimage.css ***!
  \**************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
  ".item": {
    "display": "flex",
    "justifyContent": "center",
    "alignItems": "center",
    "left": "0px",
    "top": "0px",
    "width": "454px",
    "height": "454px"
  }
}

/***/ }),

/***/ "./lib/json.js!./lib/template.js!E:\\Projects\\HarmonyProject\\OhRoundImage\\entry\\src\\main\\js\\default\\pages\\example\\example.hml":
/*!***********************************************************************************************************************************!*\
  !*** ./lib/json.js!./lib/template.js!E:/Projects/HarmonyProject/OhRoundImage/entry/src/main/js/default/pages/example/example.hml ***!
  \***********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
  "attr": {
    "debugLine": "pages/example/example:3",
    "className": "container"
  },
  "type": "div",
  "classList": [
    "container"
  ],
  "children": [
    {
      "attr": {
        "debugLine": "pages/example/example:4",
        "value": function () {return this.title}
      },
      "type": "text"
    },
    {
      "attr": {
        "debugLine": "pages/example/example:5",
        "path": "/common/dog.jpg",
        "length": "500",
        "shape": "1"
      },
      "type": "roundimage"
    }
  ]
}

/***/ }),

/***/ "./lib/json.js!./lib/template.js!E:\\Projects\\HarmonyProject\\OhRoundImage\\entry\\src\\main\\js\\default\\pages\\roundimage\\roundimage.hml":
/*!*****************************************************************************************************************************************!*\
  !*** ./lib/json.js!./lib/template.js!E:/Projects/HarmonyProject/OhRoundImage/entry/src/main/js/default/pages/roundimage/roundimage.hml ***!
  \*****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = {
  "attr": {
    "debugLine": "pages/roundimage/roundimage:1",
    "className": "item"
  },
  "type": "div",
  "classList": [
    "item"
  ],
  "children": [
    {
      "attr": {
        "debugLine": "pages/roundimage/roundimage:2",
        "src": function () {return this.path}
      },
      "type": "image",
      "style": {
        "width": function () {return (this.length) + 'px'},
        "height": function () {return (this.length) + 'px'},
        "borderRadius": function () {return (this.shape==0?this.length/2:50) + 'px'}
      }
    }
  ]
}

/***/ }),

/***/ "./lib/loader.js!E:\\Projects\\HarmonyProject\\OhRoundImage\\entry\\src\\main\\js\\default\\pages\\roundimage\\roundimage.hml?name=roundimage":
/*!*****************************************************************************************************************************************!*\
  !*** ./lib/loader.js!E:/Projects/HarmonyProject/OhRoundImage/entry/src/main/js/default/pages/roundimage/roundimage.hml?name=roundimage ***!
  \*****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var $app_template$ = __webpack_require__(/*! !./lib/json.js!./lib/template.js!./roundimage.hml */ "./lib/json.js!./lib/template.js!E:\\Projects\\HarmonyProject\\OhRoundImage\\entry\\src\\main\\js\\default\\pages\\roundimage\\roundimage.hml")
var $app_style$ = __webpack_require__(/*! !./lib/json.js!./lib/style.js!./roundimage.css */ "./lib/json.js!./lib/style.js!E:\\Projects\\HarmonyProject\\OhRoundImage\\entry\\src\\main\\js\\default\\pages\\roundimage\\roundimage.css")
var $app_script$ = __webpack_require__(/*! !./lib/script.js!./node_modules/babel-loader?presets[]=C:/Users/zhouxiyuan/AppData/Local/Huawei/OpenHarmony-SDK-2.0-Canary/js/2.2.0.0/build-tools/ace-loader/node_modules/@babel/preset-env&plugins[]=C:/Users/zhouxiyuan/AppData/Local/Huawei/OpenHarmony-SDK-2.0-Canary/js/2.2.0.0/build-tools/ace-loader/node_modules/@babel/plugin-transform-modules-commonjs&comments=false!./roundimage.js */ "./lib/script.js!./node_modules/babel-loader/lib/index.js?presets[]=C:\\Users\\zhouxiyuan\\AppData\\Local\\Huawei\\OpenHarmony-SDK-2.0-Canary\\js\\2.2.0.0\\build-tools\\ace-loader\\node_modules\\@babel\\preset-env&plugins[]=C:\\Users\\zhouxiyuan\\AppData\\Local\\Huawei\\OpenHarmony-SDK-2.0-Canary\\js\\2.2.0.0\\build-tools\\ace-loader\\node_modules\\@babel\\plugin-transform-modules-commonjs&comments=false!E:\\Projects\\HarmonyProject\\OhRoundImage\\entry\\src\\main\\js\\default\\pages\\roundimage\\roundimage.js")

$app_define$('@app-component/roundimage', [], function($app_require$, $app_exports$, $app_module$) {

$app_script$($app_module$, $app_exports$, $app_require$)
if ($app_exports$.__esModule && $app_exports$.default) {
$app_module$.exports = $app_exports$.default
}

$app_module$.exports.template = $app_template$

$app_module$.exports.style = $app_style$

})


/***/ }),

/***/ "./lib/script.js!./node_modules/babel-loader/lib/index.js?presets[]=C:\\Users\\zhouxiyuan\\AppData\\Local\\Huawei\\OpenHarmony-SDK-2.0-Canary\\js\\2.2.0.0\\build-tools\\ace-loader\\node_modules\\@babel\\preset-env&plugins[]=C:\\Users\\zhouxiyuan\\AppData\\Local\\Huawei\\OpenHarmony-SDK-2.0-Canary\\js\\2.2.0.0\\build-tools\\ace-loader\\node_modules\\@babel\\plugin-transform-modules-commonjs&comments=false!E:\\Projects\\HarmonyProject\\OhRoundImage\\entry\\src\\main\\js\\default\\pages\\example\\example.js":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./lib/script.js!./node_modules/babel-loader/lib?presets[]=C:/Users/zhouxiyuan/AppData/Local/Huawei/OpenHarmony-SDK-2.0-Canary/js/2.2.0.0/build-tools/ace-loader/node_modules/@babel/preset-env&plugins[]=C:/Users/zhouxiyuan/AppData/Local/Huawei/OpenHarmony-SDK-2.0-Canary/js/2.2.0.0/build-tools/ace-loader/node_modules/@babel/plugin-transform-modules-commonjs&comments=false!E:/Projects/HarmonyProject/OhRoundImage/entry/src/main/js/default/pages/example/example.js ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = function(module, exports, $app_require$){"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  data: {
    title: '圆形图片示例'
  }
};
exports["default"] = _default;

function requireModule(moduleName) {
  const systemList = ['system.router', 'system.app', 'system.prompt', 'system.configuration',
  'system.image', 'system.device', 'system.mediaquery', 'ohos.animator', 'system.grid', 'system.resource']
  var target = ''
  if (systemList.includes(moduleName.replace('@', ''))) {
    target = $app_require$('@app-module/' + moduleName.substring(1));
    return target;
  }
  var shortName = moduleName.replace(/@[^.]+.([^.]+)/, '$1');
  if (typeof systemplugin !== 'undefined') {
    target = systemplugin;
    for (let key of shortName.split('.')) {
      target = target[key];
      if(!target) {
        break;
      }
    }
    if (typeof target !== 'undefined') {
      return target;
    }
  }
  target = requireNapi(shortName);
  return target;
}

var moduleOwn = exports.default || module.exports;
var accessors = ['public', 'protected', 'private'];
if (moduleOwn.data && accessors.some(function (acc) {
    return moduleOwn[acc];
  })) {
  throw new Error('For VM objects, attribute data must not coexist with public, protected, or private. Please replace data with public.');
} else if (!moduleOwn.data) {
  moduleOwn.data = {};
  moduleOwn._descriptor = {};
  accessors.forEach(function(acc) {
    var accType = typeof moduleOwn[acc];
    if (accType === 'object') {
      moduleOwn.data = Object.assign(moduleOwn.data, moduleOwn[acc]);
      for (var name in moduleOwn[acc]) {
        moduleOwn._descriptor[name] = {access : acc};
      }
    } else if (accType === 'function') {
      console.warn('For VM objects, attribute ' + acc + ' value must not be a function. Change the value to an object.');
    }
  });
}}
/* generated by ace-loader */


/***/ }),

/***/ "./lib/script.js!./node_modules/babel-loader/lib/index.js?presets[]=C:\\Users\\zhouxiyuan\\AppData\\Local\\Huawei\\OpenHarmony-SDK-2.0-Canary\\js\\2.2.0.0\\build-tools\\ace-loader\\node_modules\\@babel\\preset-env&plugins[]=C:\\Users\\zhouxiyuan\\AppData\\Local\\Huawei\\OpenHarmony-SDK-2.0-Canary\\js\\2.2.0.0\\build-tools\\ace-loader\\node_modules\\@babel\\plugin-transform-modules-commonjs&comments=false!E:\\Projects\\HarmonyProject\\OhRoundImage\\entry\\src\\main\\js\\default\\pages\\roundimage\\roundimage.js":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./lib/script.js!./node_modules/babel-loader/lib?presets[]=C:/Users/zhouxiyuan/AppData/Local/Huawei/OpenHarmony-SDK-2.0-Canary/js/2.2.0.0/build-tools/ace-loader/node_modules/@babel/preset-env&plugins[]=C:/Users/zhouxiyuan/AppData/Local/Huawei/OpenHarmony-SDK-2.0-Canary/js/2.2.0.0/build-tools/ace-loader/node_modules/@babel/plugin-transform-modules-commonjs&comments=false!E:/Projects/HarmonyProject/OhRoundImage/entry/src/main/js/default/pages/roundimage/roundimage.js ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = function(module, exports, $app_require$){"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  props: {
    length: {
      "default": 100
    },
    path: {
      "default": "/common/gitee.jpg"
    },
    shape: {
      "default": 0
    }
  },
  data: {}
};
exports["default"] = _default;

function requireModule(moduleName) {
  const systemList = ['system.router', 'system.app', 'system.prompt', 'system.configuration',
  'system.image', 'system.device', 'system.mediaquery', 'ohos.animator', 'system.grid', 'system.resource']
  var target = ''
  if (systemList.includes(moduleName.replace('@', ''))) {
    target = $app_require$('@app-module/' + moduleName.substring(1));
    return target;
  }
  var shortName = moduleName.replace(/@[^.]+.([^.]+)/, '$1');
  if (typeof systemplugin !== 'undefined') {
    target = systemplugin;
    for (let key of shortName.split('.')) {
      target = target[key];
      if(!target) {
        break;
      }
    }
    if (typeof target !== 'undefined') {
      return target;
    }
  }
  target = requireNapi(shortName);
  return target;
}

var moduleOwn = exports.default || module.exports;
var accessors = ['public', 'protected', 'private'];
if (moduleOwn.data && accessors.some(function (acc) {
    return moduleOwn[acc];
  })) {
  throw new Error('For VM objects, attribute data must not coexist with public, protected, or private. Please replace data with public.');
} else if (!moduleOwn.data) {
  moduleOwn.data = {};
  moduleOwn._descriptor = {};
  accessors.forEach(function(acc) {
    var accType = typeof moduleOwn[acc];
    if (accType === 'object') {
      moduleOwn.data = Object.assign(moduleOwn.data, moduleOwn[acc]);
      for (var name in moduleOwn[acc]) {
        moduleOwn._descriptor[name] = {access : acc};
      }
    } else if (accType === 'function') {
      console.warn('For VM objects, attribute ' + acc + ' value must not be a function. Change the value to an object.');
    }
  });
}}
/* generated by ace-loader */


/***/ }),

/***/ "E:\\Projects\\HarmonyProject\\OhRoundImage\\entry\\src\\main\\js\\default\\pages\\example\\example.hml?entry":
/*!*********************************************************************************************************!*\
  !*** E:/Projects/HarmonyProject/OhRoundImage/entry/src/main/js/default/pages/example/example.hml?entry ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! !./lib/loader.js!../roundimage/roundimage.hml?name=roundimage */ "./lib/loader.js!E:\\Projects\\HarmonyProject\\OhRoundImage\\entry\\src\\main\\js\\default\\pages\\roundimage\\roundimage.hml?name=roundimage")
var $app_template$ = __webpack_require__(/*! !./lib/json.js!./lib/template.js!./example.hml */ "./lib/json.js!./lib/template.js!E:\\Projects\\HarmonyProject\\OhRoundImage\\entry\\src\\main\\js\\default\\pages\\example\\example.hml")
var $app_style$ = __webpack_require__(/*! !./lib/json.js!./lib/style.js!./example.css */ "./lib/json.js!./lib/style.js!E:\\Projects\\HarmonyProject\\OhRoundImage\\entry\\src\\main\\js\\default\\pages\\example\\example.css")
var $app_script$ = __webpack_require__(/*! !./lib/script.js!./node_modules/babel-loader?presets[]=C:/Users/zhouxiyuan/AppData/Local/Huawei/OpenHarmony-SDK-2.0-Canary/js/2.2.0.0/build-tools/ace-loader/node_modules/@babel/preset-env&plugins[]=C:/Users/zhouxiyuan/AppData/Local/Huawei/OpenHarmony-SDK-2.0-Canary/js/2.2.0.0/build-tools/ace-loader/node_modules/@babel/plugin-transform-modules-commonjs&comments=false!./example.js */ "./lib/script.js!./node_modules/babel-loader/lib/index.js?presets[]=C:\\Users\\zhouxiyuan\\AppData\\Local\\Huawei\\OpenHarmony-SDK-2.0-Canary\\js\\2.2.0.0\\build-tools\\ace-loader\\node_modules\\@babel\\preset-env&plugins[]=C:\\Users\\zhouxiyuan\\AppData\\Local\\Huawei\\OpenHarmony-SDK-2.0-Canary\\js\\2.2.0.0\\build-tools\\ace-loader\\node_modules\\@babel\\plugin-transform-modules-commonjs&comments=false!E:\\Projects\\HarmonyProject\\OhRoundImage\\entry\\src\\main\\js\\default\\pages\\example\\example.js")

$app_define$('@app-component/example', [], function($app_require$, $app_exports$, $app_module$) {

$app_script$($app_module$, $app_exports$, $app_require$)
if ($app_exports$.__esModule && $app_exports$.default) {
$app_module$.exports = $app_exports$.default
}

$app_module$.exports.template = $app_template$

$app_module$.exports.style = $app_style$

})
$app_bootstrap$('@app-component/example',undefined,undefined)

/***/ })

/******/ });