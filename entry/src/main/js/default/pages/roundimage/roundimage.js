export default {
    props: {
        length: {
            default: 100,
        },
        path: {
            default: "/common/gitee.jpg",
        },
        shape: {
            default: 0,
        },
    },
    data: {
    }
}
